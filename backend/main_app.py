from flask import Flask, render_template, jsonify, request
from estudiantes import estudiantesEndpoint
from administrador import adminEndpoints
from profesor import preseforEndpoint
from materia import materiaEndpoint
# Create the application instance
app = Flask(__name__)
app.register_blueprint(estudiantesEndpoint)
app.register_blueprint(adminEndpoints)
app.register_blueprint(preseforEndpoint)
app.register_blueprint(materiaEndpoint)


# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    PATH ENDPOINT
    """
    return render_template('index.html')



if __name__ == '__main__':
    app.run(debug=True, port=8001)
