from flask import Blueprint, request, jsonify
from connect import db
import json

from bson.objectid import ObjectId

preseforEndpoint = Blueprint('profesor', __name__)


'''Endpoint crear profesor'''
@preseforEndpoint.route('/crear_profesor', methods=["POST"])
def createTheacher():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("nombre")
        materia = data.get("materia")
        getUser = db.teacher.insert({"nombre": nombre, "materia": materia})
        if getUser:
            return jsonify({"status": 200, "msg": "usuario creado"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})

'''Endpoint autualizar profesor'''
@preseforEndpoint.route('/atu_profesor', methods=["POST"])
def actuTheacher():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("nombre")
        materia = data.get("materia")
        getUser = db.teacher.update({"nombre": nombre, "materia": materia})
        if getUser:
            return jsonify({"status": 200, "msg": "usuario actualizado"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})

'''Endpoint eliminar profesor'''
@preseforEndpoint.route('/eli_profesor', methods=["POST"])
def delTheacher():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("nombre")
        materia = data.get("materia")
        getUser = db.teacher.delete({"nombre": nombre}, {"nombre": nombre, "materia": materia})
        if getUser:
            return jsonify({"status": 200, "msg": "usuario eliminado"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})

'''Endpoint cargar profesor'''
@preseforEndpoint.route('/getPro', methods=["POST"])
def getPro():
       
    prof = db.teacher.find({})
    profJson = []
    for teacher in prof:
        obj  = {
            "nombre": teacher["nombre"],
            "materia": teacher["materia"]
        }
        profJson.append(obj)
    if profJson:
        return jsonify({"status": 200, "data": profJson})