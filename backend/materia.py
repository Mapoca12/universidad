from flask import Blueprint, request, jsonify
from connect import db
import json

from bson.objectid import ObjectId

materiaEndpoint = Blueprint('materia', __name__)


'''Endpoint crear materia'''
@materiaEndpoint.route('/crear_materia', methods=["POST"])
def createMateria():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("materia")
        profesorMate = data.get("profemat")
        descripcion = data.get("descrip")
        numeroCupos = data.get("numerocupo")
        
        getUser = db.materia.insert({"nombre": nombre, "profesorMate": profesorMate, 
        "descripcion": descripcion, "numeroCupos": numeroCupos})
        if getUser:
            return jsonify({"status": 200, "msg": "materia creada"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})


'''Endpoint Atualizar materia'''
@materiaEndpoint.route('/actu_materia', methods=["POST"])
def atuMateria():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("materia")
        profesorMate = data.get("profemat")
        descripcion = data.get("descrip")
        numeroCupos = data.get("numerocupo")
        
        getUser = db.materia.update({"nombre": nombre},{"nombre": nombre, "profesorMate": profesorMate, 
        "descripcion": descripcion, "numeroCupos": numeroCupos})
        if getUser:
            return jsonify({"status": 200, "msg": "materia atualizada"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})

'''Endpoint Eliminar materia'''
@materiaEndpoint.route('/dele_materia', methods=["POST"])
def delMateria():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("materia")
        profesorMate = data.get("profemat")
        descripcion = data.get("descrip")
        numeroCupos = data.get("numerocupo")
        
        getUser = db.students.delete({"nombre": nombre}, {"nombre": nombre, "profesorMate": profesorMate, 
        "descripcion": descripcion, "numeroCupos": numeroCupos})
        if getUser:
            return jsonify({"status": 200, "msg": "materia eliminada"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 400, "msg": "No data"})


'''Endpoint restar materia'''
@materiaEndpoint.route('/verificar_materia', methods=["POST"])
def verfyMateria():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        nombre = data.get("materia")
   
        materia = db.materia.find({"nombre": nombre})
        for match in materia:
            print(match)
            match["numeroCupos"]= int(match["numeroCupos"]) - 1
            db.materia.replace_one({"_id": ObjectId(match["_id"])}, match)
        return jsonify({"status": 200, "msg": "materia creada"})

'''Endpoint cargar materia'''
@materiaEndpoint.route('/getMaterias', methods=["POST"])
def getMaterias():
       
    materias = db.materia.find({})
    materiasJson = []
    for materia in materias:
        obj  = {
            "nombre": materia["nombre"],
            "cupo": materia["numeroCupos"]
        }
        materiasJson.append(obj)
    if materiasJson:
        return jsonify({"status": 200, "data": materiasJson})