from flask import Blueprint, request, jsonify
from connect import db
import json
from binascii import hexlify
import os

from bson.objectid import ObjectId

estudiantesEndpoint = Blueprint('estudiantes', __name__)


'''Endpoint crear estudiante'''
@estudiantesEndpoint.route('/crear_estudiante', methods=["POST"])
def createStudent():
    data = json.loads(request.data)
    print(data, "DATA")
    if data:
        user = data.get("correo")
        password = data.get("contrasena")
        key = hexlify(os.urandom(2))
        getUser = db.students.insert({"correo": user, "contrasena": password, "codigo_estudiante": key})
        if getUser:
            return jsonify({"status": 200, "msg": "usuario creado"})
        else:
            return jsonify({"status": 400})
    else:
        return jsonify({"status": 200, "msg": "No data"})




'''Endpoint login'''
@estudiantesEndpoint.route('/login', methods=["POST"])
def login():
    data = json.loads(request.data)
    if data:
        user = data.get("user")
        password = data.get("password")
        getUser = db.students.find_one({"correo": user, "contrasena": password})
        if getUser:
            return jsonify({"status": 200, "user": "student"})
        else:
            admin = db.admin.find_one({"Nombre" : user, "Password": password})
            if admin:
                return jsonify({"status": 200, "user": "admin"})
            else:    
                return jsonify({"status": 400})
    else:
        return jsonify({"status": 200, "msg": "No data"})

'''Endpoint cargar profesor'''
@estudiantesEndpoint.route('/getEst', methods=["POST"])
def getEst():
       
    prof = db.students.find({})
    profJson = []
    for students in prof:
        obj  = {
            "correo": students["correo"],
            "contrasena": students["contrasena"]
        }
        profJson.append(obj)
    if profJson:
        return jsonify({"status": 200, "data": profJson})

if __name__ == "__main__":
  db.materias.find_one({"nombre": user})