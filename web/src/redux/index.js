import { combineReducers } from "redux";
import loginReducer from "../containers/login/reducer";
import homeReducer from "../containers/home/reducer";
import estudiantesReducer from "../containers/estudiantes/reducer";
import profesorReducer from "../containers/profesor/reducer";
import materiaReducer from "../containers/materia/reducer";
import inicioestudReducer from "../containers/inicioestu/reducer";
import vistaproReducer from "../containers/vistapro/reducer";
import vistaestReducer from "../containers/vistaest/reducer";

export default combineReducers({
  login: loginReducer,
  home: homeReducer,
  estudiantes: estudiantesReducer,
  profesor: profesorReducer,
  materia: materiaReducer,
  inicioestu: inicioestudReducer,
  vistapro: vistaproReducer,
  vistaest: vistaestReducer
});