import React, { Component } from 'react';
import { Grid, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Promat extends Component {

  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/profesores") }} variant="outlined" color="primary">Crear Profesor</Button>
        </Grid>
          <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/materia") }} variant="outlined" color="primary">Crear Materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/vistaest") }} variant="outlined" color="primary">Listar de estudiantes</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">volver al inicio</Button>
        </Grid>
      </Grid >
    );
  }
} 

export default withStyles(styles)(Promat)
