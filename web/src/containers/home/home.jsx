import React, { Component } from 'react';
import { setHomeScreen } from './actions';
import { connect } from 'react-redux'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      test: "TEST"
    }
  }
  render() {
    return (
      <div className="App">
        <div>
          Hello This is a home<br></br>
          {this.state.test}
          {this.props.value}
          <button onClick={e => { this.props.history.push("/") }}></button>
          <button onClick={e => { this.props.setHomeScreen("OK") }}>OK</button>
          <button onClick={e => { this.props.setHomeScreen("Cancel") }}>Cancel</button>
        </div>
      </div >
    );
  }
}

const mapStateToProps = state => ({
  value: state.home.value
})

const mapDispatchToProps = dispatch => ({
  setHomeScreen: (value) => dispatch(setHomeScreen(value))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
