import * as constants from './constants';

let initialStates = {
  screen: "HOME",
  testOk: false
}

const homeReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.HOME_SCREEN:
      return {
        ...state,
        value: action.value
      }

    case constants.TEST:
      return {
        ...state,
        testOk: true
      }

    default: return state
  }
}

export default homeReducer;