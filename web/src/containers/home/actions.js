import * as constants from "./constants";

export const setHomeScreen = (value) => ({
  type: constants.HOME_SCREEN,
  value
})

export const test = (value) => ({
  type: constants.TEST,
  value
})