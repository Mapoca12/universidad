import * as constants from './constants';

let initialStates = {
  screen: "HOME",
  testOk: false,
  materias: []
}

const vistaproReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.HOME_SCREEN:
      return {
        ...state,
        value: action.value
      }

      case constants.GET_PROFESOR:
      return {
        ...state,
        materias: action.data
      }

    default: return state
  }
}

export default vistaproReducer;