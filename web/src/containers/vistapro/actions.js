import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const successInicio = (success) => ({
  type: constants.SUCCESS_PROFESOR,
  success
})

export const getPro = (data) => ({
  type: constants.GET_PROFESOR,
  data
})

 /// FETCH FUNCTIONS
 export const getFetchVpro = (materia) => {
  return function (dispatch) {
    return servicesProvider("getPro", "POST", { })
      .then((data) => {
        console.log("DATA", data);
      }).catch((err) => {
        console.log("ERROR", err);
      });
    }
  }