import * as constants from './constants';

let initialStates = {
  value: "EMPTY",
  success: false,
  typeUser: ""
}

const loginReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.TEST:
      return {
        ...state,
        value: action.value
      }

    case constants.SUCCESS_LOGIN:
      return {
        ...state,
        success: action.success,
        typeUser: action.typeUser
      }

    default: return state
  }
}

export default loginReducer;