import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const testAction = (value) => ({
  type: constants.TEST,
  value
})

export const successLogin = (success, typeUser) => ({
  type: constants.SUCCESS_LOGIN,
  success,
  typeUser
})

/// FETCH FUNCTIONS
export const fetchLogin = (user, password) => {
  return function (dispatch) {
    return servicesProvider("login", "POST", { user: user, password: password })
      .then((data) => {
        console.log("DATA", data);
        if (data){
          console.log(data.user)
          dispatch(successLogin(true, data.user));
        }
      }).catch((err) => {
        dispatch(successLogin(false, ""));
        console.log("ERROR", err);
      });
  }
}