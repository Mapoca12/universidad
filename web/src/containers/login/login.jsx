import React, { Component } from 'react';
import { successLogin, fetchLogin } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: ""
    }
    this.props.successLogin(false, "")
  }

  componentDidUpdate(){
    if (this.props.typeUser === "admin"){
      this.props.history.push("/pro-mat")
    }else if (this.props.typeUser === "student"){
      this.props.history.push("/pro-mat-est")
    }
  }

  sendLogin = () => {
    if (this.state.user !== "" && this.state.password !== "") {
      this.props.fetchLogin(this.state.user, this.state.password);
      console.log("LOGGIN")
    }
      
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <TextField
            placeholder="Usuario"
            className={classes.input}
            onChange={e => { this.setState({ user: e.target.value }) }}
            onBlur={e => { this.setState({ user: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Contraseña"
            type="password"
            onChange={e => { this.setState({ password: e.target.value }) }}
            onBlur={e => { this.setState({ password: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.sendLogin() }} variant="outlined" color="primary">Iniciar sesion</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button} onClick={e => { this.props.history.push("/estudiantes") }} variant="outlined" color="secondary">Crear cuenta</Button>
        </Grid>
        {this.props.sucessLogin ? <label>Usuario logeado</label> : ""}
      </Grid >
    );
  }
} withStyles(styles)

const mapStateToProps = state => ({
  sucessLogin: state.login.success,
  typeUser: state.login.typeUser,
})

const mapDispatchToProps = dispatch => ({
  successLogin: (success, type) => dispatch(successLogin(success, type)),
  fetchLogin: (user, password) => dispatch(fetchLogin(user, password)),

})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login))
