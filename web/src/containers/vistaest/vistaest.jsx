import React, { Component } from 'react';
import { getFetchVest } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Vistaest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nombre: "",
      materia: ""
    }
  }

  crearCuenta = () => { 
        this.props.getFetchVest(this.state.materia)
        alert("Ya vio que estudiante le toca la materia", "Asignar")
        this.props.history.push("/")
  }

  getCount = (e) => {
    this.props.materias.map((value) =>{
      if (value.nombre === e.target.value){
        this.setState({nombre: value.materia})
        this.setState({materia: e.target.value})
      }
    })
  }


  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <select 
            placeholder="Nombre del Estudiante"
            className={classes.input}
            onChange={e => { this.getCount(e) }}
            onBlur={e => { this.setState({ materia: e.target.value }) }}
          >
          {
            this.props.materias.map((value) =>{
              return <option value={value.nombre}>{value.nombre}</option>
            })
          }
          </select>
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            value={this.state.materia}
            placeholder="Materias"
            disabled = {true}
          />
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.crearCuenta() }} variant="outlined" color="primary">Volver Al Inicio</Button>
        </Grid>
      </Grid >
    );
  }
}

const mapStateToProps = state => ({
  materias: state.inicioestu.materias
})

const mapDispatchToProps = dispatch => ({
  getFetchVest: () => dispatch(getFetchVest())
})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Vistaest))