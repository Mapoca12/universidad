import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const successInicio = (success) => ({
  type: constants.SUCCESS_MATERIA,
  success
})

export const getMaterias = (data) => ({
  type: constants.GET_MATERIAS,
  data
})

/// FETCH FUNCTIONS
export const inimateria = (materia , cuposdispo) => {
  return function (dispatch) {
    return servicesProvider("crear_materia", "POST", {materia : materia, cuposdispo: cuposdispo})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successInicio(true));
        }
      }).catch((err) => {
        dispatch(successInicio(false));
        console.log("ERROR", err);
      });
    }
  }

  /// FETCH FUNCTIONS
export const getFetchMaterias = () => {
  return function (dispatch) {
    return servicesProvider("getMaterias", "POST", {})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(getMaterias(data.data));
        }
      }).catch((err) => {
        console.log("ERROR", err);
      });
    }
  }

 /// FETCH FUNCTIONS
 export const getFetchMenos = (materia) => {
  return function (dispatch) {
    return servicesProvider("verificar_materia", "POST", { materia: materia })
      .then((data) => {
        console.log("DATA", data);
      }).catch((err) => {
        console.log("ERROR", err);
      });
    }
  }