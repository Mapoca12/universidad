import React, { Component } from 'react';
import { getFetchMaterias, getFetchMenos } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Inicioestu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      materia: "",
      cuposdispo: 0
    }
  }

  componentDidMount(){
    this.props.getFetchMaterias()
  }

  crearCuenta = () => { 
        this.props.getFetchMenos(this.state.materia)
        alert("Ya se asigno ala materia", "Asignar")
        this.props.history.push("/")
  }


  getCount = (e) => {
    this.props.materias.map((value) =>{
      if (value.nombre === e.target.value){
        this.setState({cuposdispo: value.cupo})
        this.setState({materia: e.target.value})
      }
    })
  }


  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <select 
            placeholder="Nombre de la materia"
            className={classes.input}
            onChange={e => { this.getCount(e) }}
            onBlur={e => { this.setState({ materia: e.target.value }) }}
          >
          {
            this.props.materias.map((value) =>{
              return <option value={value.nombre}>{value.nombre}</option>
            })
          }
          </select>
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            value={this.state.cuposdispo}
            placeholder="Cupos Disponibles"
            disabled = {true}
          />
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.crearCuenta() }} variant="outlined" color="primary">Asignar Materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">Volver al Inicio</Button>
        </Grid>
      </Grid >
    );
  }
}

const mapStateToProps = state => ({
  materias: state.inicioestu.materias
})

const mapDispatchToProps = dispatch => ({
  getFetchMaterias: () => dispatch(getFetchMaterias()),
  getFetchMenos: (materia) => dispatch(getFetchMenos(materia))
})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Inicioestu))