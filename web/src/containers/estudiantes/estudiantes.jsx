import React, { Component } from 'react';
import { testAction, crearestudiante } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Estudiantes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      correo: "",
      contrasena: "",
      confirmarcontrasena: ""
    }
  }


  crearCuenta = () => { 
    if (this.state.correo !== "" && this.state.contrasena !== "" && this.state.confirmarcontrasena !== "") { 
      if (this.state.contrasena === this.state.confirmarcontrasena)
      this.props.crearestudiante(this.state.correo, this.state.contrasena, 
        this.state.confirmarcontrasena);
        this.props.history.push("/")
    }
  }


  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <TextField
            placeholder="Usuario"
            className={classes.input}
            onChange={e => { this.setState({ correo: e.target.value }) }}
            onBlur={e => { this.setState({ correo: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Contraseña"
            type="password"
            onChange={e => { this.setState({ contrasena: e.target.value }) }}
            onBlur={e => { this.setState({ contrasena: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Confirmacion contraseña"
            type="password"
            onChange={e => { this.setState({ confirmarcontrasena: e.target.value }) }}
            onBlur={e => { this.setState({ confirmarcontrasena: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Codigo estudiante"
            disabled = {true}
          />
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.crearCuenta() }} variant="outlined" color="primary">Crear cuenta</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">Volver al Inicio</Button>
        </Grid>
      </Grid >
    );
  }
}

const mapStateToProps = state => ({
  value: state.home.value
})

const mapDispatchToProps = dispatch => ({
  testAction: (value) => dispatch(testAction(value)),
  crearestudiante: (correo, contrasena,confirmarcontrasena) => dispatch(crearestudiante(correo, contrasena,confirmarcontrasena)),
})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Estudiantes))