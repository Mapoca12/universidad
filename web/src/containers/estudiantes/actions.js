import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const testAction = (value) => ({
  type: constants.TEST,
  value
})

export const successEstud = (value) => ({
  type: constants.SUCCESS_ESTU,
  value
})

/// FETCH FUNCTIONS
export const crearestudiante = (correo, contrasena, confirmarcontrasena) => {
  return function (dispatch) {
    return servicesProvider("crear_estudiante", "POST", {correo : correo, contrasena: contrasena, 
      confirmarcontrasena: confirmarcontrasena})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successEstud(true));
        }
      }).catch((err) => {
        dispatch(successEstud(false));
        console.log("ERROR", err);
      });
  }
}