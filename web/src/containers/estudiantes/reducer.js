import * as constants from './constants';

let initialStates = {
  value: "EMPTY",
  success: false
}

const estudiantesReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.TEST:
      return {
        ...state,
        value: action.value
      }

    case constants.SUCCESS_ESTU:
      return {
        ...state,
        success: action.success
      }

    default: return state
  }
}

export default estudiantesReducer;