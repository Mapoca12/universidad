import * as constants from './constants';

let initialStates = {
  value: "EMPTY",
  success: false
}

const materiaReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.TEST:
      return {
        ...state,
        value: action.value
      }

    case constants.SUCCESS_MATERIA:
      return {
        ...state,
        success: action.success
      }

    default: return state
  }
}

export default materiaReducer;