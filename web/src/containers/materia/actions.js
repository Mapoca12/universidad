import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const testAction = (value) => ({
  type: constants.TEST,
  value
})

export const successMateria = (success) => ({
  type: constants.SUCCESS_MATERIA,
  success
})

/// FETCH FUNCTIONS
export const crearmateria = (materia, profemat, descrip, numerocupo) => {
  return function (dispatch) {
    return servicesProvider("crear_materia", "POST", {materia : materia, profemat: profemat, 
          descrip: descrip , numerocupo: numerocupo})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successMateria(true));
        }
      }).catch((err) => {
        dispatch(successMateria(false));
        console.log("ERROR", err);
      });
  }
}

/// FETCH FUNCTIONS
export const atumateria = (materia, profemat, descrip, numerocupo) => {
  return function (dispatch) {
    return servicesProvider("actu_materia", "POST", {materia : materia, profemat: profemat, 
          descrip: descrip , numerocupo: numerocupo})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successMateria(true));
        }
      }).catch((err) => {
        dispatch(successMateria(false));
        console.log("ERROR", err);
      });
  }
}

/// FETCH FUNCTIONS
export const delmateria = (materia, profemat, descrip, numerocupo) => {
  return function (dispatch) {
    return servicesProvider("dele_materia", "POST", {materia : materia, profemat: profemat, 
          descrip: descrip , numerocupo: numerocupo})
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successMateria(true));
        }
      }).catch((err) => {
        dispatch(successMateria(false));
        console.log("ERROR", err);
      });
  }
}
