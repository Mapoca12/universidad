import React, { Component } from 'react';
import { testAction, crearmateria, atumateria, delmateria } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Materia extends Component {

  constructor(props) {
    super(props);
    this.state = {
      materia: "",
      profemat: "",
      descrip: "",
      numerocupo: ""
    }
  }

  crearCuenta = () => { 
    if (this.state.materia !== "" && this.state.profemat !== ""
        && this.state.descrip !== "" && this.state.numerocupo !== "") { 
      this.props.crearmateria(this.state.materia, this.state.profemat, 
      this.state.descrip, this.state.numerocupo);
    }
  }

  atumate = () => { 
    if (this.state.materia !== "" && this.state.profemat !== ""
        && this.state.descrip !== "" && this.state.numerocupo !== "") { 
      this.props.crearmateria(this.state.materia, this.state.profemat, 
      this.state.descrip, this.state.numerocupo);
    }
  }

  delmate = () => { 
    if (this.state.materia !== "" && this.state.profemat !== ""
        && this.state.descrip !== "" && this.state.numerocupo !== "") { 
      this.props.crearmateria(this.state.materia, this.state.profemat, 
      this.state.descrip, this.state.numerocupo);
    }
  }


  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <TextField
            placeholder="Materia"
            className={classes.input}
            onChange={e => { this.setState({ materia: e.target.value }) }}
            onBlur={e => { this.setState({ materia: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Profesor Materia"
            onChange={e => { this.setState({ profemat: e.target.value }) }}
            onBlur={e => { this.setState({ profemat: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            placeholder="Descripcion"
            className={classes.input}
            onChange={e => { this.setState({ descrip: e.target.value }) }}
            onBlur={e => { this.setState({ descrip: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Numero de Cupos"
            onChange={e => { this.setState({ numerocupo: e.target.value }) }}
            onBlur={e => { this.setState({ numerocupo: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.crearCuenta() }} variant="outlined" color="primary">Crear Materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.atumate() }} variant="outlined" color="primary">Actuliza Materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.delmate() }} variant="outlined" color="primary">Eliminar Materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">Volver Al inicio</Button>
        </Grid>
      </Grid >
    );
  }
} withStyles(styles)

const mapStateToProps = state => ({
  sucessLogin: state.login.success
})

const mapDispatchToProps = dispatch => ({
  testAction: (value) => dispatch(testAction(value)),
  crearmateria: (materia, profemat,descrip, numerocupo) => dispatch(crearmateria(materia, profemat,descrip, numerocupo)),
  atumateria: (materia, profemat,descrip, numerocupo) => dispatch(atumateria(materia, profemat,descrip, numerocupo)),
  delmateria: (materia, profemat,descrip, numerocupo) => dispatch(delmateria(materia, profemat,descrip, numerocupo)),
})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Materia))
