import React, { Component } from 'react';
import { Grid, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Promate extends Component {

  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/inicioestu") }} variant="outlined" color="primary">Agregar Materia</Button>
        </Grid>
          <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/vistapro") }} variant="outlined" color="primary">Verlistado de profesores por materia</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">volver al inicio</Button>
        </Grid>
      </Grid >
    );
  }
} 

export default withStyles(styles)(Promate)
