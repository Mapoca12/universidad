import * as constants from "./constants";
import servicesProvider from "../../services/servicesProvider";

export const testAction = (value) => ({
  type: constants.TEST,
  value
})

export const successProfesor = (success) => ({
  type: constants.SUCCESS_PROFESOR,
  success
})

/// FETCH FUNCTIONS
export const crearprofesor = (Nombre, materia) => {
  return function (dispatch) {
    return servicesProvider("crear_profesor", "POST", { nombre: Nombre, materia: materia })
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successProfesor(true));
        }
      }).catch((err) => {
        dispatch(successProfesor(false));
        console.log("ERROR", err);
      });
  }
}

/// FETCH FUNCTIONS
export const autprofesor = (Nombre, materia) => {
  return function (dispatch) {
    return servicesProvider("atu_profesor", "POST", { nombre: Nombre, materia: materia })
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successProfesor(true));
        }
      }).catch((err) => {
        dispatch(successProfesor(false));
        console.log("ERROR", err);
      });
  }
}

/// FETCH FUNCTIONS
export const eliprofesor = (Nombre, materia) => {
  return function (dispatch) {
    return servicesProvider("eli_profesor", "POST", { nombre: Nombre, materia: materia })
      .then((data) => {
        console.log("DATA", data);
        if (data){
          dispatch(successProfesor(true));
        }
      }).catch((err) => {
        dispatch(successProfesor(false));
        console.log("ERROR", err);
      });
  }
}