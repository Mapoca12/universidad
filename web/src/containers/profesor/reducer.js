import * as constants from './constants';

let initialStates = {
  value: "EMPTY",
  success: false
}

const profesorReducer = (state = initialStates, action) => {
  switch (action.type) {
    case constants.TEST:
      return {
        ...state,
        value: action.value
      }

    case constants.SUCCESS_PROFESOR:
      return {
        ...state,
        success: action.success
      }

    default: return state
  }
}

export default profesorReducer;