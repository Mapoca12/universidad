import React, { Component } from 'react';
import { testAction, crearprofesor, eliprofesor, autprofesor } from './actions';
import { connect } from 'react-redux'
import { Grid, TextField, Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  button: {
    width: "100%",

  },
  input: {
    width: '100%',
  },
});

class Profesor extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nombre: "",
      materia: ""
    }
  }

  crearCuenta = () => { 
    if (this.state.nombre !== "" && this.state.materia !== "") { 
      this.props.crearprofesor(this.state.nombre, this.state.materia);
    }
  }

  autupro = () => { 
    if (this.state.nombre !== "" && this.state.materia !== "") { 
      this.props.autprofesor(this.state.nombre, this.state.materia);
    }
  }

  delpro = () => { 
    if (this.state.nombre !== "" && this.state.materia !== "") { 
      this.props.eliprofesor(this.state.nombre, this.state.materia);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <TextField
            placeholder="Nombre"
            className={classes.input}
            onChange={e => { this.setState({ nombre: e.target.value }) }}
            onBlur={e => { this.setState({ nombre: e.target.value }) }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.input}
            placeholder="Materia"
            onChange={e => { this.setState({ materia: e.target.value }) }}
            onBlur={e => { this.setState({ materia: e.target.value }) }}
          />
        </Grid>
          <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.crearCuenta() }} variant="outlined" color="primary">Crear Profesor</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.autupro() }} variant="outlined" color="primary">Actualizar Profesor</Button>
        </Grid>
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.delpro() }} variant="outlined" color="primary">Eliminar Profesor</Button>
        </Grid>       
        <Grid item xs={12}>
          <Button className={classes.button}
            onClick={e => { this.props.history.push("/") }} variant="outlined" color="primary">Volver Al inicio</Button>
        </Grid>                 
      </Grid >
    );
  }
} withStyles(styles)

const mapStateToProps = state => ({
  sucessLogin: state.login.success
})

const mapDispatchToProps = dispatch => ({
  testAction: (value) => dispatch(testAction(value)),
  crearprofesor: (nombre, materia) => dispatch(crearprofesor(nombre, materia)),
  autprofesor: (nombre, materia) => dispatch(autprofesor(nombre, materia)),
  eliprofesor: (nombre, materia) => dispatch(eliprofesor(nombre, materia)),
})

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Profesor))
