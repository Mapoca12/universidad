import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './redux'
import Root from "./router";
import thunk from "redux-thunk";
const store = createStore(rootReducer, applyMiddleware(thunk))

ReactDOM.render(<Root store={store}></Root>, document.getElementById('root'));
