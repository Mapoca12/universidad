let headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'

}
const serviceProvider = (endpoint, method, body) => {
  return new Promise((resolve, reject) => {
    fetch(endpoint, { method: method, headers: headers, body: JSON.stringify(body), mode: "no-cors" })
      .then(response => response.json())
      .then(res => {
        console.log('res: ', res);
        resolve(res);
      }).catch((err) => {
        console.log("ERROR --->", err);
        reject(err)
      });
  });
}

export default serviceProvider;