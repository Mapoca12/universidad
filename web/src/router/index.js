import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Login from '../containers/login/login.jsx';
import Home from "../containers/home/home.jsx";
import Estudiantes from "../containers/estudiantes/estudiantes.jsx";
import Profesores from "../containers/profesor/profesores.jsx";
import Materia from "../containers/materia/materia.jsx";
import Promat from "../containers/pro-mat/pro-mat.jsx";
import Inicioestu from "../containers/inicioestu/inicioestud.jsx";
import Vistapro from "../containers/vistapro/vistapro.jsx";
import Vistaest from "../containers/vistaest/vistaest.jsx";
import Vmpe from "../containers/pro-mat-est/pro-mat-est.jsx";


const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/dashboard" component={Home} />
        <Route path="/estudiantes" component={Estudiantes} />
        <Route path="/profesores" component={Profesores} />
        <Route path="/materia" component={Materia} />
        <Route path="/pro-mat" component={Promat} />
        <Route path="/inicioestu" component={Inicioestu} />
        <Route path="/vistapro" component={Vistapro} />
        <Route path="/vistaest" component={Vistaest} />
        <Route path="/pro-mat-est" component={Vmpe} />
        
        
      </Switch>
    </Router>
  </Provider>
)

export default Root;